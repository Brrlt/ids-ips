# Projet hacking - Exploit CVE-2019-18792

Le serveur est basé sur le code de krillwow, disponible ici : https://github.com/kirillwow/ids_bypass/

# Procédure d'installation 

## MATERIEL :
	VirtualBox
	Une VM ubuntu20.04
	Une VM autre (ubuntu22.04 dans nos tests)

## INSTALLATION SURICATA :
	Sur une VM ubuntu20.04 (la compilation de suricata5.0.0 ne fonctionne pas sur ubuntu22.04).
	Installer les dépendances :
	- sudo apt-get -y install libpcre3 libpcre3-dbg libpcre3-dev \
	  build-essential autoconf automake libtool libpcap-dev libnet1-dev \
	  libyaml-0-2 libyaml-dev zlib1g zlib1g-dev libcap-ng-dev libcap-ng0 \
	  make libmagic-dev libjansson-dev libjansson4 pkg-config \
	  libnetfilter-queue-dev libnetfilter-queue1 libnfnetlink-dev libnfnetlink0
	Executer les commandes suivantes :
		- wget "http://www.openinfosecfoundation.org/download/suricata-5.0.0.tar.gz"
		- tar -xvzf "suricata-5.0.0.tar.gz"
		- cd suricata-5.0.0
		- ./configure --enable-nfqueue --prefix=/usr --sysconfdir=/etc --localstatedir=/var
		- make
		- sudo make install
		- sudo make install-conf
## INSTALLATION DU SERVEUR :
	Sur la seconde VM, copier server.h, server_attack.c, server_no_attack.c et makefile. (ou git clone https://gitlab.com/Brrlt/ids-ips).
	Dans le repertoire contenant les fichiers :
	- make
## MISE EN PLACE DU RESEAU :
	Une fois les deux étapes précédentes effectuer, un accès à internet n'est plus nécéssaire.
	S'assurer que les deux VM sont sur le même réseau. (Réseau interne virtualbox dans nos tests).
## LANCER SURICATA :
	Sur la machine suricata :
		Ecrire la règle suivante :
		- nano /var/lib/suricata/rules/suricata.rules
		copier alert tcp any any -> any any (msg:"THIS_IS_A_TEST_MALWARE"; content:"THIS_IS_A_TEST"; nocase; metadata: former_category MALWARE;)
		Lancer le moteur :
		- sudo suricata -c /etc/suricata/suricata.yaml -i interface --init-errors-fatal
		Afficher les logs en temps réel :
		- tail -f /var/log/suricata/fast.log
## LANCER LE SERVEUR :
	Sur la machine serveur :
		-sudo iptables -A OUTPUT -p tcp --sport 80 --tcp-flags RST RST -j DROP
		-sudo ./server_no_attack -i vboxnet0(interface) -p 80
## LANCER L'ATTAQUE :
	Sur la machine suricata :
		Lancer firefox et accéder à l'adresse IP du serveur.
		Le message THIS_IS_A_TEST s'affiche sur la page web, et une alerte est levée par Suricata.
	Sur la machine serveur :
		Arreter le serveur, puis :
			-sudo ./server_attack -i vboxnet0(interface) -p 80
	Sur la machine suricata :
		Retourner sur firefox et acceder de nouveau à la page (dans un nouvel onglet ou via control+f5 pour eviter que la page ne soit chargée depuis la cache).
		Le même message s'affiche sur la page web, mais aucune alerte n'est levée par Suricata.