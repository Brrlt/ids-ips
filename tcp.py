import socket
import struct
import array

class TCP:
    def __init__(self,
                 src_host,
                 src_port,
                 dst_host,
                 dst_port,
                 flags=0):
        self.src_host = src_host
        self.src_port = src_port
        self.dst_host = dst_host
        self.dst_port = dst_port
        self.flags = flags

    def build(self):
            packet = struct.pack(
                '!HHIIBBHHH',
                self.src_port,  # Source Port
                self.dst_port,  # Destination Port
                0,              # Sequence Number
                0,              # Acknoledgement Number
                5 << 4,         # Data Offset
                self.flags,     # Flags
                8192,           # Window
                0,              # Checksum (initial value)
                0               # Urgent pointer
            )
            pseudo_hdr = struct.pack(
                '!4s4sHH',
                socket.inet_aton(self.src_host),    # Source Address
                socket.inet_aton(self.dst_host),    # Destination Address
                socket.IPPROTO_TCP,                 # Protocol ID
                len(packet)                         # TCP Length
            )

            checksum = chksum(pseudo_hdr + packet)
            packet = packet[:16] + struct.pack('H', checksum) + packet[18:]
            return packet

def chksum(packet):
    if len(packet) % 2 != 0:
        packet += b'\0'    
    res = sum(array.array("H", packet))
    res = (res >> 16) + (res & 0xffff)
    res += res >> 16    
    return (~res) & 0xffff

dst = '192.168.56.112'
packet = TCP(
    '192.168.56.1',
    55555,
    dst,
    8000,
    0b000111101  # Merry Christmas!
)
s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)

s.sendto(packet.build(), (dst, 0))

print(packet)