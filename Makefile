CC		    = gcc
CFLAGS		= -c -Wall -D_GNU_SOURCE -g
LDFLAGS		= -lpcap
SOURCES		= server_attack.c server_no_attack.c
INCLUDES	= -I.
TARGET		= server_attack server_no_attack

all: $(SOURCES) $(TARGET)

server_attack: server_attack.o
	$(CC) server_attack.o -o $@ $(LDFLAGS)
    
server_no_attack: server_no_attack.o
	$(CC) server_no_attack.o -o $@ $(LDFLAGS)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) $< -o $@

clean:
	rm -rf *.o $(TARGET)
