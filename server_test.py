"""
 Implements a simple HTTP/1.0 Server

"""

import socket
import struct
import array


# Define socket host and port
SERVER_HOST = '0.0.0.0'
SERVER_PORT = 8000

# Create socket
s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(3))
s.bind(("vboxnet0", 0))

#print('Listening on port %s ...' % SERVER_PORT)


def chksum(packet):
    if len(packet) % 2 != 0:
        packet += b'\0'    
    res = sum(array.array("H", packet))
    res = (res >> 16) + (res & 0xffff)
    res += res >> 16    
    return (~res) & 0xffff

class TCP:
    def __init__(self,
                 src_host,
                 src_port,
                 dst_host,
                 dst_port,
                 data,
                 ack_nbr,
                 flags=0):
        self.src_host = src_host
        self.src_port = src_port
        self.dst_host = dst_host
        self.dst_port = dst_port
        self.ack_nbr = ack_nbr
        self.data = data
        self.flags = flags

    def build(self):
            packet = struct.pack(
                '!HHIIBBHHH',
                self.src_port,  # Source Port
                self.dst_port,  # Destination Port
                0,              # Sequence Number
                self.ack_nbr,              # Acknoledgement Number
                5 << 4,         # Data Offset
                self.flags,     # Flags
                8192,           # Window
                0,              # Checksum (initial value)
                0,               # Urgent pointer
            ) + self.data.encode()
            pseudo_hdr = struct.pack(
                '!4s4sHH',
                socket.inet_aton(self.src_host),    # Source Address
                socket.inet_aton(self.dst_host),    # Destination Address
                socket.IPPROTO_TCP,                 # Protocol ID
                len(packet)                         # TCP Length
            )

            checksum = chksum(pseudo_hdr + packet)
            packet = packet[:16] + struct.pack('H', checksum) + packet[18:]
            return packet

packet = TCP(
    '192.168.56.1',
    5000,
    '192.168.56.112',
    5000,
    "",
    10,
    0b000000001
)

def data_received(data):
        packet = data

        ethernet_header = packet[0:14]
        ethernet_detail = struct.unpack("!6s6s2s", ethernet_header)
        print(ethernet_detail)

while True:    
    # Wait for client connections
    v = s.recv(8000)
    print(v)
    data_received(v)

    # client_connection, client_address = server_socket.accept()

    # # Get the client request
    # request = client_connection.recv(1024).decode()
    # print(request)

    # client_connection
    # # Send HTTP response
    # response = 'THIS_IS_A_TEST'
    # server_socket.sendto(packet.build(), ('192.168.56.112', 0))
    # client_connection.sendall(response.encode())
    # client_connection.close()

# Close socket
server_socket.close()

# dst = '192.168.56.112'

# s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)

# packet = TCP(
#     '192.168.56.1',
#     5000,
#     dst,
#     5000,
#     "",
#     0,
#     0b000000010
# )

# s.sendto(packet.build(), (dst, 0))

# s.sendto(packet.build(), (dst, 0))

# packet = TCP(
#     '192.168.56.1',
#     5000,
#     dst,
#     5000,
#     "THIS_IS_A_TEST",
#     10,
#     0b000011000

# )

# s.sendto(packet.build(), (dst, 0))

# print(packet)